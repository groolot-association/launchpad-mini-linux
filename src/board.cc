/* This file is part of the 'lmapper' program
 * (https://framagit.org/groolot-association/launchpad-mini-linux)
 *
 * Copyright (C) 2019 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "board.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <csignal>
#include <thread>
#include <atomic>
#include <chrono>
#include <stdexcept>
using namespace std::literals::chrono_literals;

#include <json/json.h>

#include "config.h"
#include "cli.h"

launchpad::Cli *cli = launchpad::Cli::instance();

volatile std::atomic<bool> done;

static void signal_handler(int)
{
  done = true;
}

launchpad::Board::Board(bool _flash_enable) :
  width(8),
  flash_enable(_flash_enable)
{
  try
  {
    midiin = new RtMidiIn(RtMidi::Api::LINUX_ALSA, PACKAGE " MIDI in from launchpad");
    midireturn = new RtMidiOut(RtMidi::Api::LINUX_ALSA, PACKAGE " MIDI return to launchpad");
    midiout = new RtMidiOut(RtMidi::Api::LINUX_ALSA, PACKAGE " MIDI out to application");
  }
  catch(RtMidiError &error)
  {
    error.printMessage();
    exit(EXIT_FAILURE);
  }

  listDevices();
  std::vector<unsigned char> message;
  std::cout << "Initialize launchpad board state on "
            << (cli->return_port >= 0 ? midireturn->getPortName(cli->return_port) : "virtual port")
            << "."
            << std::endl;

  if(cli->input_port >= 0)
  {
    midiin->openPort(cli->input_port, PACKAGE "-input");
  }
  else
  {
    midiin->openVirtualPort(PACKAGE "-virtual-input");
  }

  if(cli->return_port >= 0)
  {
    midireturn->openPort(cli->return_port, PACKAGE "-return");
  }
  else
  {
    midireturn->openVirtualPort(PACKAGE "-virtual-return");
  }

  if(cli->output_port >= 0)
  {
    midiout->openPort(cli->output_port, PACKAGE "-output");
  }
  else
  {
    midiout->openVirtualPort(PACKAGE "-virtual-output");
  }

  midiin->ignoreTypes();
  loadJson(cli->layout);
  turnOnAllLEDs();
  std::this_thread::sleep_until(std::chrono::high_resolution_clock::now() + 1s);
  reset();
  configureFlash();
  refreshLEDs();
}

launchpad::Board::~Board()
{
  if(cli->reset_on_exit)
  {
    reset();
  }

  delete midiin;
  delete midireturn;
  delete midiout;
}

void launchpad::Board::listDevices()
{
  if(cli->list_devices)
  {
    std::string portName;
    uint16_t nPorts = 0;
    // Check inputs.
    nPorts = midiin->getPortCount();
    std::cout << "There are "
              << nPorts
              << " MIDI input sources available."
              << std::endl;

    for(uint16_t port = 0; port < nPorts; ++port)
    {
      try
      {
        portName = midiin->getPortName(port);
      }
      catch(RtMidiError &error)
      {
        error.printMessage();
      }

      std::cout << "  Input Port #"
                << port
                << ": "
                << portName << std::endl;
    }

    // Check outputs.
    nPorts = midiout->getPortCount();
    std::cout << std::endl
              << "There are "
              << nPorts
              << " MIDI output ports available."
              << std::endl;

    for(uint16_t port = 0; port < nPorts; ++port)
    {
      try
      {
        portName = midiout->getPortName(port);
      }
      catch(RtMidiError &error)
      {
        error.printMessage();
      }

      std::cout << "  Output Port #"
                << port
                << ": "
                << portName
                << std::endl;
    }
  }
}

void launchpad::Board::addPad(launchpad::Pad _pad)
{
  pads[_pad.getKey()] = _pad;
}

std::map<uint8_t, launchpad::Pad> *launchpad::Board::getPads()
{
  return &pads;
}

void launchpad::Board::loadJson(std::string filename)
{
  Json::Value json;
  std::ifstream input_file;
  std::string errors;
  input_file.open(filename);
  Json::CharReaderBuilder builder;
  builder["collectComments"] = false;
  std::string default_color("green_low");
  std::string default_flags("normal");
  bool default_state(false);
  bool default_enable(false);

  if(!Json::parseFromStream(builder, input_file, &json, &errors))
  {
    json["default"]["color"] = default_color;
    json["default"]["flags"] = default_flags;
    json["default"]["state"] = default_state;
    default_enable = true;
    json["default"]["enable"] = default_enable;
    std::cerr << "Use standard layout configuration due to Json file following error(s):"
              << std::endl << errors << std::endl;
  }
  else
  {
    default_color = json["default"].get("color", default_color).asString();
    default_flags = json["default"].get("flags", default_flags).asString();
    default_state = json["default"].get("state", default_state).asBool();
    default_enable = json["default"].get("enable", default_enable).asBool();
  }

  input_file.close();

  /* Initialize collection of pads */
  for(unsigned int j = 0; j < width; ++j)
  {
    for(unsigned int i = 0; i < width; ++i)
    {
      launchpad::Pad pad(i,
                         j,
                         default_state,
                         default_enable);
      pad.velocity.setColor(default_color);
      pad.velocity.setFlags(default_flags);
      addPad(pad);
    }
  }

  for(auto json_pad : json["pads"])
  {
    if(!json_pad.isMember("posX") or !json_pad.isMember("posY"))
    {
      std::ostringstream output(std::ios_base::app);
      output << "Json layout file malformed." << std::endl;

      if(!json_pad.isMember("posX"))
      {
        output << "Please fill correctly the \"posX\" pad property" << std::endl;
      }

      if(!json_pad.isMember("posY"))
      {
        output << "Please fill correctly the \"posY\" pad property" << std::endl;
      }

      output << "Responsible pad definition:"
             << std::endl << json_pad << std::endl;
      throw std::range_error(output.str().c_str());
    }

    uint8_t key = launchpad::Pad::getKey(json_pad["posX"].asUInt(),
                                         json_pad["posY"].asUInt());
    pads[key].state = json_pad.get("state", default_state).asBool();
    pads[key].enable = true;
    pads[key].velocity.setColor(json_pad.get("color", default_color).asString());
    pads[key].velocity.setFlags(json_pad.get("flag", default_flags).asString());
  }
}

void launchpad::Board::run()
{
  if(std::signal(SIGINT, signal_handler) != SIG_ERR &&
     std::signal(SIGABRT, signal_handler) != SIG_ERR &&
     std::signal(SIGTERM, signal_handler) != SIG_ERR)
  {
    // Periodically check input queue.
    std::clog << "Reading MIDI input from "
              << (cli->input_port >= 0 ? midiin->getPortName(cli->input_port) : "virtual port")
              << "." << std::endl;
    unsigned int nBytes;
    std::vector<unsigned char> in_message;
    std::vector<unsigned char> message;
    using hrclock = std::chrono::high_resolution_clock;
    hrclock::time_point next_time_point;

    while(!done)
    {
      next_time_point = hrclock::now() + 10ms;

      if(cli->input_port == -1 or midiin->isPortOpen())
      {
        midiin->getMessage(&in_message);
        message = in_message;
        nBytes = in_message.size();

        if(nBytes > 0)
        {
          if((cli->return_port == -1 or midireturn->isPortOpen())
             and (cli->output_port == -1 or midiout->isPortOpen()))
          {
            if(!cli->filter)
            {
              // ByPass MIDI to output
              midiout->sendMessage(&message);
            }

            if(nBytes  == 3)
            {
              if(in_message[0] == MIDI_NOTE_ON)
              {
                if(pads.find(in_message[1]) != pads.end()
                   and pads[in_message[1]].enable)
                {
                  if(in_message[2] != 0x0)
                  {
                    pads[in_message[1]].toggleState();

                    if(cli->filter)
                    {
                      // Send control value to out MIDI port
                      message[0] = pads[message[1]].state ? MIDI_NOTE_ON : MIDI_NOTE_OFF;
                      midiout->sendMessage(&message);
                    }

                    // Send visual information to return MIDI port
                    message = pads[message[1]].getMIDImessage();
                    midireturn->sendMessage(&message);
                  }
                }
              }
            }
          }
        }
      }

      std::this_thread::sleep_until(next_time_point);
    }
  }
}

void launchpad::Board::configureFlash()
{
  if(flash_enable and (cli->return_port == -1 or midireturn->isPortOpen()))
  {
    std::vector<unsigned char> message = {MIDI_CC, 0x0, 0b00101000};
    midireturn->sendMessage(&message);
  }
}

void launchpad::Board::reset()
{
  if(midireturn->isPortOpen())
  {
    std::vector<unsigned char> message = {MIDI_CC, 0x0, 0x0};
    midireturn->sendMessage(&message);
  }
}

void launchpad::Board::turnOnAllLEDs()
{
  if(cli->return_port == -1 or midireturn->isPortOpen())
  {
    std::vector<unsigned char> message = {MIDI_CC, 0x0, 0x7f};
    midireturn->sendMessage(&message);
  }
}

void launchpad::Board::refreshLEDs()
{
  if(cli->return_port == -1 or midireturn->isPortOpen())
  {
    std::vector<unsigned char> message;

    for(auto pad : pads)
    {
      if(pad.second.enable)
      {
        message = pad.second.getMIDImessage();
        midireturn->sendMessage(&message);
        message.clear();
      }
    }
  }
}

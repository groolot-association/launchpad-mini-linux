/* This file is part of the 'lmapper' program
 * (https://framagit.org/groolot-association/launchpad-mini-linux)
 *
 * Copyright (C) 2019 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#ifndef SRC_VELOCITY_H
#define SRC_VELOCITY_H

#include <cstdint>
#include <string>
#include <map>

namespace launchpad {
namespace mini {
enum COLOR
{
  OFF =        0b00000000,

  RED_OFF =    0b00000000,
  RED_LOW =    0b00000001,
  RED_MID =    0b00000010,
  RED_HIGH =   0b00000011,

  GREEN_OFF =  0b00000000,
  GREEN_LOW =  0b00010000,
  GREEN_MID =  0b00100000,
  GREEN_HIGH = 0b00110000,

  // Amber is composed of RED + GREEN
  AMBER_OFF =  0b00000000,
  AMBER_LOW =  0b00010001,
  AMBER_MID =  0b00100010,
  AMBER_HIGH = 0b00110011,
};

enum FLAG
{
  DOUBLE_BUFFERING = 0b00000000,
  LED_FLASHING =     0b00001000,  // Must have been configured on board first
  NORMAL =           0b00001100,
};

class Velocity
{
 private:
  COLOR color;
  FLAG flags;

 public:
  explicit Velocity();
  explicit Velocity(COLOR _color);
  explicit Velocity(COLOR _color, FLAG _flags);

  void setColor(COLOR _color);
  void setColor(std::string _color);
  void setFlags(FLAG _flags);
  void setFlags(std::string _flags);
  uint8_t getMIDIvelocity();
};
}
}

#endif

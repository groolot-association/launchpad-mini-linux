/* This file is part of the 'lmapper' program
 * (https://framagit.org/groolot-association/launchpad-mini-linux)
 *
 * Copyright (C) 2019 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#ifndef SRC_CLI_H
#define SRC_CLI_H

#include <cstdint>
#include <string>

#include <tclap/CmdLine.h>
#include <RtMidi.h>

namespace launchpad {
class Cli
{
 private:
  Cli();
  static Cli *singleton;
  int argc;
  char **argv;
  bool validate_MIDI_port(int32_t midi_port, bool input = true);


 public:
  int32_t input_port;
  int32_t return_port;
  int32_t output_port;
  bool list_devices;
  bool filter;
  bool reset_on_exit;
  std::string layout;

  static Cli *instance();
  void setArgs(int _argc, char **_argv);
};
}

#endif

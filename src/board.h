/* This file is part of the 'lmapper' program
 * (https://framagit.org/groolot-association/launchpad-mini-linux)
 *
 * Copyright (C) 2019 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#ifndef SRC_BOARD_H
#define SRC_BOARD_H

#include <map>
#include <cstdint>

#include <RtMidi.h>

#include "pad.h"

namespace launchpad {

/** class Board
 *
 * \brief Represent a launchpad board
 */
class Board
{
 private:
  uint8_t width;
  bool flash_enable;
  RtMidiIn *midiin;  ///< Pointer handling the MIDI input from launchpad
  RtMidiOut *midireturn;  ///< Pointer handling the MIDI return information to launchpad (the aim of this program)
  RtMidiOut *midiout;  ///< Pointer handling the MIDI output to the remote application to control
  std::map<uint8_t, launchpad::Pad> pads;  ///< Collection of pads, indexed by MIDI key
  void loadJson(std::string filename);
  void configureFlash();
  void turnOnAllLEDs();
  void reset();
  void refreshLEDs();

 public:
  explicit Board(bool _flash_enable = false);
  ~Board();
  void listDevices();
  void addPad(launchpad::Pad _pad);  ///< Add a Pad to the collection to the correct key mapping
  std::map<uint8_t, launchpad::Pad> *getPads();
  void run();
};
}

#endif

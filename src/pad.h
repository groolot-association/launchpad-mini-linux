/* This file is part of the 'lmapper' program
 * (https://framagit.org/groolot-association/launchpad-mini-linux)
 *
 * Copyright (C) 2019 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#ifndef SRC_PAD_H
#define SRC_PAD_H

#include <cstdint>
#include <vector>

#include "velocity.h"

namespace launchpad {

/** class Pad
 *
 * \brief Represent a MIDI Note pad on a launchpad board
 */
class Pad
{
 private:
  uint8_t posX;
  uint8_t posY;

 public:
  launchpad::mini::Velocity velocity;
  bool state;
  bool enable;

  explicit Pad(bool _state = false,
               bool _enable = false);
  explicit Pad(uint8_t _posX,
               uint8_t _posY,
               bool _state = false,
               bool _enable = false);
  uint8_t getKey();
  static uint8_t getKey(uint8_t _posX, uint8_t _posY);
  bool toggleState();
  bool toggleEnable();
  std::vector<unsigned char> getMIDImessage();
};
}

#endif

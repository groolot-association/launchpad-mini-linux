/* This file is part of the 'lmapper' program
 * (https://framagit.org/groolot-association/launchpad-mini-linux)
 *
 * Copyright (C) 2019 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "pad.h"

#include "config.h"

launchpad::Pad::Pad(bool _state, bool _enable) :
  posX(0),
  posY(0),
  velocity(),
  state(_state),
  enable(_enable)
{}

launchpad::Pad::Pad(uint8_t _posX, uint8_t _posY, bool _state, bool _enable) :
  posX(_posX),
  posY(_posY),
  velocity(),
  state(_state),
  enable(_enable)
{}

uint8_t launchpad::Pad::getKey()
{
  return getKey(posX, posY);
}

uint8_t launchpad::Pad::getKey(uint8_t _posX, uint8_t _posY)
{
  return (uint8_t)(LP_ROW_OFFSET * _posY + _posX);
}

bool launchpad::Pad::toggleState()
{
  state = !state;
  return state;
}

bool launchpad::Pad::toggleEnable()
{
  enable = !enable;
  return enable;
}

std::vector<unsigned char> launchpad::Pad::getMIDImessage()
{
  std::vector<unsigned char> message;
  message.push_back(state ? MIDI_NOTE_ON : MIDI_NOTE_OFF);
  message.push_back(getKey());
  message.push_back(velocity.getMIDIvelocity());
  return message;
}

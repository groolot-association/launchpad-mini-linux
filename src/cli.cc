/* This file is part of the 'lmapper' program
 * (https://framagit.org/groolot-association/launchpad-mini-linux)
 *
 * Copyright (C) 2019 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "cli.h"

#include <RtMidi.h>

#include "config.h"

launchpad::Cli *launchpad::Cli::singleton = nullptr;

launchpad::Cli *launchpad::Cli::instance()
{
  if(!singleton)
  {
    singleton = new Cli();
  }

  return singleton;
}

launchpad::Cli::Cli()
{}

void launchpad::Cli::setArgs(int _argc, char **_argv)
{
  argc = _argc;
  argv = _argv;

  try
  {
    TCLAP::CmdLine cmd("MIDI surface Novation Launchpad Mini LEDs mapper",
                       ' ',
                       PACKAGE_VERSION);
    TCLAP::ValueArg<int> input_port_Arg("i", "input-port",
                                        "Input MIDI port number",
                                        false, -1, "INTEGER", cmd);
    TCLAP::ValueArg<int> return_port_Arg("r", "return-port",
                                         "Input MIDI port number",
                                         false, -1, "INTEGER", cmd);
    TCLAP::ValueArg<int> output_port_Arg("o", "output-port",
                                         "Input MIDI port number",
                                         false, -1, "INTEGER", cmd);
    TCLAP::SwitchArg list_devices_SwArg("l", "list-devices",
                                        "List IN/OUT MIDI devices",
                                        cmd, false);
    TCLAP::SwitchArg filter_SwArg("F", "no-filter",
                                  "Don't filter input MIDI messages to output based on pad states",
                                  cmd, true);
    TCLAP::SwitchArg reset_on_exit_SwArg("R", "no-reset-on-exit",
                                         "Don't reset/blank LEDs on exit",
                                         cmd, true);
    TCLAP::ValueArg<std::string> layout_Arg("f", "layout",
                                            "Layout filename",
                                            false, std::string("layout.json"), "FILENAME", cmd);
    // Parse the args.
    cmd.parse(argc, argv);
    // Get the value parsed by each arg.
    input_port = input_port_Arg.getValue();
    return_port = return_port_Arg.getValue();
    output_port = output_port_Arg.getValue();
    list_devices = list_devices_SwArg.getValue();
    filter = filter_SwArg.getValue();
    reset_on_exit = reset_on_exit_SwArg.getValue();
    layout = layout_Arg.getValue();

    if(!validate_MIDI_port(input_port, true))
    {
      input_port = -1;
    }

    if(!validate_MIDI_port(return_port, false))
    {
      return_port = -1;
    }

    if(!validate_MIDI_port(output_port, false))
    {
      output_port = -1;
    }
  }
  catch(TCLAP::ArgException &e)     // catch any exceptions
  {
    std::cerr << "error: " << e.error() << " for " << e.argId() << std::endl;
  }
}

bool launchpad::Cli::validate_MIDI_port(int32_t midi_port, bool input)
{
  if(midi_port >= 0)
  {
    void *midiPort;

    if(input)
    {
      midiPort = new RtMidiIn();
    }
    else
    {
      midiPort = new RtMidiOut();
    }

    bool valid = static_cast<unsigned int>(midi_port + 1) <= static_cast<RtMidi *>(midiPort)->getPortCount();

    if(input)
    {
      delete static_cast<RtMidiIn *>(midiPort);
    }
    else
    {
      delete static_cast<RtMidiOut *>(midiPort);
    }

    return valid;
  }
  else
  {
    // If negative midi_port number, virtual port case!
    return true;
  }
}

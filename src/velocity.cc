/* This file is part of the 'lmapper' program
 * (https://framagit.org/groolot-association/launchpad-mini-linux)
 *
 * Copyright (C) 2019 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "velocity.h"

launchpad::mini::Velocity::Velocity() :
  color(launchpad::mini::COLOR::GREEN_LOW),
  flags(launchpad::mini::FLAG::NORMAL)
{}

launchpad::mini::Velocity::Velocity(launchpad::mini::COLOR _color) :
  color(_color),
  flags(launchpad::mini::FLAG::NORMAL)
{}

launchpad::mini::Velocity::Velocity(launchpad::mini::COLOR _color,
                                    launchpad::mini::FLAG _flags) :
  color(_color),
  flags(_flags)
{}

void launchpad::mini::Velocity::setColor(launchpad::mini::COLOR _color)
{
  color = _color;
}

void launchpad::mini::Velocity::setColor(std::string _color)
{
  if(_color == "red_low")
  {
    color = launchpad::mini::RED_LOW;
  }
  else if(_color == "red_mid")
  {
    color = launchpad::mini::RED_MID;
  }
  else if(_color == "red_high" or _color == "red")
  {
    color = launchpad::mini::RED_HIGH;
  }
  else if(_color == "green_low")
  {
    color = launchpad::mini::GREEN_LOW;
  }
  else if(_color == "green_mid")
  {
    color = launchpad::mini::GREEN_MID;
  }
  else if(_color == "green_high" or _color == "green")
  {
    color = launchpad::mini::GREEN_HIGH;
  }
  else if(_color == "amber_low")
  {
    color = launchpad::mini::AMBER_LOW;
  }
  else if(_color == "amber_mid")
  {
    color = launchpad::mini::AMBER_MID;
  }
  else if(_color == "amber_high" or _color == "amber")
  {
    color = launchpad::mini::AMBER_HIGH;
  }
  else
  {
    color = launchpad::mini::OFF;
  }
}

void launchpad::mini::Velocity::setFlags(launchpad::mini::FLAG _flags)
{
  flags = _flags;
}

void launchpad::mini::Velocity::setFlags(std::string _flags)
{
  if(_flags == "double_buffering")
  {
    flags = launchpad::mini::DOUBLE_BUFFERING;
  }
  else if(_flags == "led_flashing")
  {
    flags = launchpad::mini::LED_FLASHING;
  }
  else
  {
    flags = launchpad::mini::NORMAL;
  }
}

uint8_t launchpad::mini::Velocity::getMIDIvelocity()
{
  return color + flags;
}
